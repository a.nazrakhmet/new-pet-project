import React from 'react';
import City from './components/City';
import './App.css';
const App: React.FC = () => (
  <div>
    <City />
    <p className="copyright">
      Copyright &copy; Nazrakhmet Aikun. All rights reserved.
    </p>
  </div>
);
export default App;
