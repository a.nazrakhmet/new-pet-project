import React from 'react';
import { Card } from 'react-bootstrap';
import { WeatherData } from '../types/types';
import '../App.css';
interface IProps {
  weatherData: WeatherData;
}

const CityCard: React.FC<IProps> = ({ weatherData }) => {
  return (
    <div>
      <Card className="cards">
        <Card.Img
          className="card-img"
          variant="top"
          src={`http://openweathermap.org/img/w/${weatherData?.weather[0].icon}.png`}
          alt="imgicon"
        />
        <Card.Body>
          <Card.Title>
            {weatherData?.name} | {weatherData?.sys.country}
            <hr></hr>
          </Card.Title>
          <Card.Text>
            <h6>{weatherData?.weather[0].main}</h6>
            <h5>{Math.round(Number(weatherData?.main.temp) - 273)}°C </h5>
            <h6>
              Min: {Math.round(Number(weatherData?.main.temp_min) - 273)}°C
            </h6>
            <h6>
              Max: {Math.round(Number(weatherData?.main.temp_max) - 273)}°C
            </h6>
            <h6>Humidity: {weatherData?.main.humidity}%</h6>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

export default CityCard;
