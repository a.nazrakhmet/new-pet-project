import React, { useEffect, useState } from 'react';
import { capitals } from '../api/weatherapi';
import { WeatherData } from '../types/types';
import { getWeatherData } from '../api/weatherapi';
import CityCard from './CityCard';
import '../App.css';

const Cities: React.FC = () => {
  const [citiesData, setCitiesData] = useState<WeatherData[]>();

  const getArrayData = async (array: Array<string>) => {
    let responses: WeatherData[] = [];
    for (var i = 0; i < array.length; i++) {
      const data = await getWeatherData(array[i]);
      responses.push(data);
    }
    setCitiesData(responses);
  };

  useEffect(() => {
    getArrayData(capitals);
  }, []);

  return (
    <div>
      <div className="cities">
        {citiesData?.map((obj) => (
          <CityCard key={obj.id} weatherData={obj}></CityCard>
        ))}
      </div>
    </div>
  );
};
export default Cities;
