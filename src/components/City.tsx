/* eslint-disable no-useless-catch */
import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { getWeatherData } from '../api/weatherapi';
import { WeatherData } from '../types/types';
import { InputGroup, Button } from 'react-bootstrap';
import CityCard from './CityCard';
import '../App.css';

const City: React.FC = () => {
  const [weatherData, setWeatherData] = useState<WeatherData>();
  const [city, setCity] = useState('London');

  const getData = async () => {
    try {
      const data = await getWeatherData(city);
      setWeatherData(data);
    } catch (error) {
      throw error;
    }
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="city">
      <div className="city-input">
        <InputGroup>
          <input
            className="input"
            type="text"
            value={city}
            onChange={(e) => setCity(e.target.value)}
            placeholder="Enter your city name"
          />
          <Button
            className="button-city"
            variant="outline-secondary"
            type="button"
            onClick={() => getData()}
          >
            Search
          </Button>
        </InputGroup>
      </div>
      <div className="one-city">
        <CityCard weatherData={weatherData!} />
      </div>
    </div>
  );
};

export default City;
