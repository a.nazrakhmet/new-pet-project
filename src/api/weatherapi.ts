import axios from 'axios';
// import qs from 'qs';
const baseUrl = 'http://api.openweathermap.org/data/2.5/weather?';
import { WeatherData } from '../types/types';
const apiKey = 'b78be4f5ae66eede4f1bf416a45f4a27';

export const getWeatherData = async (cityname: string) => {
  const { data } = await axios.get<WeatherData>(baseUrl, {
    params: {
      q: cityname,
      appid: apiKey
    }
  });
  return data;
};

export const capitals = [
  'Astana',
  'Beijing',
  'Washington',
  'Moscow',
  'Seoul',
  'Minsk',
  'Prague',
  'Vienna',
  'Yerevan',
  'Brussels',
  'Copenhagen',
  'Berlin',
  'Dublin',
  'Oslo',
  'Lisbon',
  'Singapore',
  'Stockholm',
  'Seoul',
  'Algiers',
  'Andorra la Vella',
  'Manama',
  'Nassau',
  'Dhaka',
  'Bridgetown',
  'Tegucigalpa',
  'Rome',
  'Amman',
  'Nairobi',
  'Bishkek',
  'Beirut'
];
